export default {
  widgets: [
    {
      name: 'sanity-tutorials',
      options: {
        templateRepoId: 'sanity-io/sanity-template-gatsby-portfolio'
      }
    },
    {name: 'structure-menu'},
    {
      name: 'project-info',
      options: {
        __experimental_before: [
          {
            name: 'netlify',
            options: {
              description:
                'NOTE: Because these sites are static builds, they need to be re-deployed to see the changes when documents are published.',
              sites: [
                {
                  buildHookId: '5ed8f3dc6616f713b6d84743',
                  title: 'Sanity Studio',
                  name: 'sanity-gatsby-portfolio-studio-m6g3kzhy',
                  apiId: 'deb9c145-dbbf-43af-932d-1753f669f899'
                },
                {
                  buildHookId: '5ed8f3dca9898c978964ccc3',
                  title: 'Portfolio Website',
                  name: 'sanity-gatsby-portfolio-web-oyedmzs8',
                  apiId: 'c749aaae-e461-4401-a42a-0214cde5787a'
                }
              ]
            }
          }
        ],
        data: [
          {
            title: 'GitHub repo',
            value: 'https://github.com/kallyas/sanity-gatsby-portfolio',
            category: 'Code'
          },
          {
            title: 'Frontend',
            value: 'https://sanity-gatsby-portfolio-web-oyedmzs8.netlify.app',
            category: 'apps'
          }
        ]
      }
    },
    {name: 'project-users', layout: {height: 'auto'}},
    {
      name: 'document-list',
      options: {title: 'Recent projects', order: '_createdAt desc', types: ['sampleProject']},
      layout: {width: 'medium'}
    }
  ]
}
